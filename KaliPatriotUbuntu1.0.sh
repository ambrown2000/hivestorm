#!/bin/bash
if [ $UID -ne 0]
then
  echo "Better run this as root, bud"
  exit
fi
errorLog = "~/Desktop/errorLog"
verbose = "~/Desktop/verboseOutput"


apt install libgnome2-bin -y
xterm -e 'apt-get install axel -y && apt-get install aria2 -y && add-apt-repository ppa:apt-fast/stable && apt-get update && apt-get -y install apt-fast'
#xterm -e 'apt-get install aria2 -y'
#xterm -e 'add-apt-repository ppa:apt-fast/stable'
#xterm -e 'apt-get update'
#xterm -e 'apt-get -y install apt-fast'

cat > /etc/apt-fast.conf << EOF
###################################################################
# CONFIGURATION OPTIONS
###################################################################
# Every item has a default value besides MIRRORS (which is unset).

# Use aptitude, apt-get, or apt?
# Note that apt-get is used as a fallback for outputting the
# package URI list for e.g. aptitude, which can't do this
# Optionally add the FULLPATH to apt-get or apt-rpm or aptitude
# e.g. /usr/bin/aptitude
#
# Default: apt-get
#
_APTMGR=apt


# Enable DOWNLOADBEFORE to suppress apt-fast confirmation dialog and download
# packages directly.
#
# Default: dialog enabled
#
DOWNLOADBEFORE=true


# Choose mirror list to speed up downloads from same archive. To select some
# mirrors take a look at your distribution's archive mirror lists.
# Debian: http://www.debian.org/mirror/list
# Ubuntu: https://launchpad.net/ubuntu/+archivemirrors
#
# It is required to add mirrors in the sources.list to this array as well, so
# apt-fast can destinguish between different distributions.
#
# Examples:
#
# Different distributions (as in operating systems):
#
# sources.list:
# deb http://deb.debian.org/debian/ unstable main non-free contrib
# deb http://de.archive.ubuntu.com/ubuntu/ bionic main universe
#
# apt-fast.conf:


MIRRORS=('http://mirror.enzu.com/ubuntu/', 'http://deb.debian.org/debian','http://ftp.debian.org/debian,http://ftp2.de.debian.org/debian,http://ftp.de.debian.org/debian,ftp://ftp.uni-kl.de/debian')
#           'http://archive.ubuntu.com/ubuntu,http://de.archive.ubuntu.com/ubuntu,http://ftp.halifax.rwth-aachen.de/ubuntu,http://ftp.uni-kl.de/pub/linux/ubuntu,http://mirror.informatik.uni-mannheim.de/pub/linux/distributions/ubuntu/' )
#
#
# Single distribution:
#
# sources.list:
# deb http://fr.archive.ubuntu.com/ubuntu/ bionic main
# deb http://fr.archive.ubuntu.com/ubuntu/ artful main
#
# apt-fast.conf:
# MIRRORS=( 'http://fr.archive.ubuntu.com/ubuntu,http://bouyguestelecom.ubuntu.lafibre.info/ubuntu,http://mirror.ovh.net/ubuntu,http://ubuntu-archive.mirrors.proxad.net/ubuntu' )
#
# Default: disabled
#
#MIRRORS=( 'none' )


# Maximum number of connections
# You can use this value in _DOWNLOADER command. Escape with ${}: ${_MAXNUM}
#
# Default: 5
#
_MAXNUM=25


# Maximum number of connections per server
# Default: 10
#
_MAXCONPERSRV=25


# Download file using given number of connections
# If more than N URIs are given, first N URIs are used and remaining URIs are used for backup.
# If less than N URIs are given, those URIs are used more than once so that N connections total are made simultaneously.
#
_SPLITCON=8


# Split size i.e. size of each piece
# Possible Values: 1M-1024M
#
_MINSPLITSZ=1M


# Piece selection algorithm to use
# Available values are: default, inorder, geom
# default: selects piece so that it reduces the number of establishing connection, reasonable for most cases
# inorder: selects pieces in sequential order starting from first piece
# geom: selects piece which has minimum index like inorder, but it exponentially increasingly keeps space from previously selected pieces
#
_PIECEALGO=default


# Downloadmanager listfile
# You can use this value in _DOWNLOADER command. Escape with ${}: ${DLLIST}
#
# Default: /tmp/apt-fast.list
#
DLLIST='/tmp/apt-fast.list'


# Download command to use. Temporary download list is designed for aria2. But
# you can choose another download command or download manager. It has to
# support following input file syntax (\t is tab character):
#
# # Comment
# MIRROR1\tMIRROR2\tMIRROR3...
#  out=FILENAME1
# MIRROR1\tMIRROR2\tMIRROR3...
#  out=FILENAME2
# ...
#
# Examples:
# aria2c with a proxy (set username, proxy, ip and password!)
# _DOWNLOADER='aria2c --no-conf -c -j ${_MAXNUM} -x ${_MAXCONPERSRV} -s ${_SPLITCON} --min-split-size=${_MINSPLITSZ} --stream-piece-selector=${_PIECEALGO} --http-proxy=http://username:password@proxy_ip:proxy_port -i ${DLLIST} --connect-timeout=600 --timeout=600 -m0 --header "Accept: */*"'
#
# Default: _DOWNLOADER='aria2c --no-conf -c -j ${_MAXNUM} -x ${_MAXCONPERSRV} -s ${_SPLITCON} --min-split-size=${_MINSPLITSZ} --stream-piece-selector=${_PIECEALGO} -i ${DLLIST} --connect-timeout=600 --timeout=600 -m0 --header "Accept: */*"'
#
_DOWNLOADER='aria2c --no-conf -c -j ${_MAXNUM} -x ${_MAXCONPERSRV} -s ${_SPLITCON} --min-split-size=${_MINSPLITSZ} --stream-piece-selector=${_PIECEALGO} -i ${DLLIST} --connect-timeout=600 --timeout=600 -m0 --header "Accept: */*"'


# Temporary download folder for download manager.
#
# Default: /var/cache/apt/apt-fast
#
DLDIR='/var/cache/apt/apt-fast'


# APT archives cache directory
#
# Default /var/cache/apt/archives
# (APT configuration items Dir::Cache and Dir::Cache::archives)
#
APTCACHE='/var/cache/apt/archives'


# apt-fast colors
# Colors are disabled when not using a terminal.
#
# Default colors are:
#  cGreen='\e[0;32m'
#  cRed='\e[0;31m'
#  cBlue='\e[0;34m'
#  endColor='\e[0m'
EOF

#users
  touch standard
  touch admins
  cat /etc/passwd | cut -d':' -f1,3 | grep -E "[1-2]..." | cut -d':' -f1 > allUsers
  chmod 777 *

  read -p "\nEnter the password you want to use: " pass

  gnome-open . 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
  echo -ne "\nAdd the users to the files. Y when done [Y/n] "
  read option


  #admins
  for user in $(cat admins | grep -vi password | grep -vi you); do  #can copy paste the list as is. it will ignore passwords and the default user (you)
    usermod -aG adm $user
    usermod -aG sudo $user
    echo -e "\nChanging $user's password to $pass"
    echo -e "$pass\n$pass" | passwd $user 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
  done

  #standard users
  for user in $(cat standard); do
    gpasswd -d $user adm
    gpasswd -d $user sudo
    echo -e "\nChanging $user's password to $pass"
    echo -e "$pass\n$pass" | passwd $user 1> /dev/null 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
  done


  #delete bad users
  mkdir ~/Desktop/delUsers
  for user in $(cat allUsers); do
    if [ `grep -o $user admins standard | wc -l` -eq 0 ]
    then
      cp -r /home/$user ~/Desktop/delUsers/ 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
      deluser $user 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
      echo -e "\n$user Deleted"
    fi
  done


#firewall
    sudo apt-get -y install ufw 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    echo -e "\n UFW installed"

    sudo apt-get -y install gufw2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    echo -e "\n GUFW2 installed"

    sudo ufw enable 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    echo -e "\n UFW enabled"

    sudo ufw logging high 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    echo -e "\n UFW logging high"
#denying ports
    sudo ufw deny 23
    sudo ufw deny 2049
    sudo ufw deny 515
    sudo ufw deny 111
    sudo ufw deny 7100
#allowing ports
    sudo ufw allow out 443
    sudo ufw allow out 8080 #for the outgoing communication with scoring and readme


#network inspections
    echo -e "lsof Results:" > ~/Desktop/ports.txt
    sudo lsof -i -n -P >> ~/Desktop/ports.txt
    echo -e "\n \nNetstat Results:" > ~/Desktop/ports.txt
    sudo netstat -tulpn >> ~/Desktop/ports.txt


# installing good programs
    echo -e "\napt-get update running"
    apt-get update 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput

    echo -e "\nInstalling good programs"
    apt-get install -y chkrootkit clamav rkhunter apparmor apparmor-profiles mhash gksudo wget bum libpam-cracklib vim 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput

    echo -e "\nRunning chkrootkit"
    chkrootkit > ~/Desktop/chkrootkit.txt

    echo -e "\nRunning clamscan"
    sudo clamscan -r -l > ~/Desktop/clamav.txt

    echo -e "\nRunning rkhunter"
    sudo rkhunter -sk --checkall -l  > ~/Desktop/rkhunter.txt


# Media Files

    echo -e "\nSearching for prohibited files. Output saved to prohibitedfiles.txt"

    echo -e "\nmp3 files:" > ~/Desktop/prohibitedfiles.txt
    find / -name '*.mp3' >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    echo -e "\nmov files:" >> ~/Desktop/prohibitedfiles.txt
    find / -name '*.mov' >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    echo -e "\nmp4 files:" >> ~/Desktop/prohibitedfiles.txt
    find / -name '*.mp4' >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    echo -e "\navi files:" >> ~/Desktop/prohibitedfiles.txt
    find / -name '*.avi' >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    echo -e "\nmpg files:" >> ~/Desktop/prohibitedfiles.txt
    find / -name '*.mpg' >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    echo -e "\nmpeg files:" >> ~/Desktop/prohibitedfiles.txt
    find / -name '*.mpeg' >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    echo -e "\nflac files:" >> ~/Desktop/prohibitedfiles.txt
    find / -name '*.flac' >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    echo -e "\nm4a files:" >> ~/Desktop/prohibitedfiles.txt
    find / -name '*.m4a' >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    echo -e "\nflv files:" >> ~/Desktop/prohibitedfiles.txt
    find / -name '*.flv' >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    echo -e "\nogg files:" >> ~/Desktop/prohibitedfiles.txt
    find / -name '*.ogg' >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    echo -e "\ngif files:" >> ~/Desktop/prohibitedfiles.txt
    find /home -name '*.gif' >> ~/Desktop/prohibitedfiles.txt
    echo -e "\npng files:" >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    find /home -name '*.png' >> ~/Desktop/prohibitedfiles.txt
    echo -e "\njpg files:" >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    find /home -name '*.jpg' >> ~/Desktop/prohibitedfiles.txt
    echo -e "\njpeg files:" >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    find /home -name '*.jpeg' >> ~/Desktop/prohibitedfiles.txt
    echo -e "\nm4b files:" >> ~/Desktop/prohibitedfiles.txt 2>> ~/Desktop/errorLog
    find /home -name '*.m4b' >> ~/Desktop/prohibitedfiles.txt
    find / -name '*.txt' > ~/Desktop/textfiles.txt 2>> ~/Desktop/errorLog


# system upgrades
    #echo -e "\napt-get update"
    gnome-terminal -e apt-get -y update 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput

    #echo -e "\napt-get upgrade"
    #gnome-terminal -e "apt -y upgrade"

    echo -e "\napt-get dist-upgrade"
    apt-get -y dist-upgrade 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput

    echo -e "\napt-get install coreutils"
    apt-get install --reinstall coreutils 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput

    echo -e "\napt-get install -f"
    gnome-terminal -e apt-get -y install -f 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput

#Hacking Programs

    echo -e "\nRemoving hacking programs"
    sudo apt-get -y purge john 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge medusa 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge hydra 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge ophcrack 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge remmina 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge minetest 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge qbittorrent 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge utorrent 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge ctorrent 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge ktorrent 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge rtorrent 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge freeciv 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge deluge 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge transmission-gtk 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge transmission-common 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge tixati 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge frostwise 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge vuze 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge irssi 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge talk 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge telnet 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge wireshark 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge nmap 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge netcat 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge netcat-openbsd  2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge netcat-traditional 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge netcat-ubuntu 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y purge netcat-minimal 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    sudo apt-get -y autoremove 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput

# finding netcat
    echo -e "\nSaving nc location to ncoutput.txt"
    whereis nc > ~/Desktop/ncoutput.txt

# Password Policies

echo -e "\nSetting pam password policies"
cat << EOF > /etc/pam.d/common-password
password    [success=1 default=ignore]	pam_unix.so obscure sha512 remember=5 minlen=8 ocredit = -1 decredit = -1 lcredit = -1 ucredit = -1
password	requisite			pam_deny.so
password	required			pam_permit.so
password	optional	pam_gnome_keyring.so
EOF

echo -e "\nSetting login policies"
cat << EOF > /etc/login.defs
FAILLOG_ENAB		yes
LOG_UNKFAIL_ENAB	no
LOG_OK_LOGINS		no
SYSLOG_SU_ENAB		yes
SYSLOG_SG_ENAB		yes
FTMP_FILE	/var/log/btmp
SU_NAME		su
HUSHLOGIN_FILE	.hushlogin
ENV_SUPATH	PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV_PATH	PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
TTYGROUP	tty
TTYPERM		0600
ERASECHAR	0177
KILLCHAR	025
UMASK		022
PASS_MAX_DAYS	30
PASS_MIN_DAYS	10
PASS_WARN_AGE	7
UID_MIN			 1000
UID_MAX			60000
GID_MIN			 1000
GID_MAX			60000
LOGIN_RETRIES		5
LOGIN_TIMEOUT		60
CHFN_RESTRICT		rwh
DEFAULT_HOME	yes
USERGROUPS_ENAB yes
ENCRYPT_METHOD SHA512
EOF

# Manage Crontab
    #echo "reset crontab"
    #crontab -r
    cd /etc/
    #/bin/rm -f cron.deny at.deny
    echo root >cron.allow
    echo root >at.allow
    /bin/chown root:root cron.allow at.allow
    /bin/chmod 644 cron.allow at.allow

# ip tables
    apt-get install -y iptables
    apt-get install -y iptables-persistent

    iptables -t nat -F
    iptables -t mangle -F
    iptables -t nat -X
    iptables -t mangle -X
    iptables -F
    iptables -X
    iptables -P INPUT DROP
    iptables -P FORWARD DROP
    iptables -P OUTPUT ACCEPT
    ip6tables -t nat -F
    ip6tables -t mangle -F
    ip6tables -t nat -X
    ip6tables -t mangle -X
    ip6tables -F
    ip6tables -X
    ip6tables -P INPUT DROP
    ip6tables -P FORWARD DROP
    ip6tables -P OUTPUT DROP

    iptables -A INPUT -p tcp -s 0/0 -d 0/0 --dport 23 -j DROP         #Block Telnet
    iptables -A INPUT -p tcp -s 0/0 -d 0/0 --dport 2049 -j DROP       #Block NFS
    iptables -A INPUT -p udp -s 0/0 -d 0/0 --dport 2049 -j DROP       #Block NFS
    iptables -A INPUT -p tcp -s 0/0 -d 0/0 --dport 6000:6009 -j DROP  #Block X-Windows
    iptables -A INPUT -p tcp -s 0/0 -d 0/0 --dport 7100 -j DROP       #Block X-Windows font server
    iptables -A INPUT -p tcp -s 0/0 -d 0/0 --dport 515 -j DROP        #Block printer port
    iptables -A INPUT -p udp -s 0/0 -d 0/0 --dport 515 -j DROP        #Block printer port
    iptables -A INPUT -p tcp -s 0/0 -d 0/0 --dport 111 -j DROP        #Block Sun rpc/NFS
    iptables -A INPUT -p udp -s 0/0 -d 0/0 --dport 111 -j DROP        #Block Sun rpc/NFS
    iptables -A INPUT -p all -s localhost -i eth0 -j DROP
    iptables -A INPUT -s 127.0.0.0/8 -i firefox -j DROP
    iptables -A INPUT -s 0.0.0.0/8 -j DROP
    iptables -A INPUT -s 100.64.0.0/10 -j DROP
    iptables -A INPUT -s 169.254.0.0/16 -j DROP
    iptables -A INPUT -s 192.0.0.0/24 -j DROP
    iptables -A INPUT -s 192.0.2.0/24 -j DROP
    iptables -A INPUT -s 198.18.0.0/15 -j DROP
    iptables -A INPUT -s 198.51.100.0/24 -j DROP
    iptables -A INPUT -s 203.0.113.0/24 -j DROP
    iptables -A INPUT -s 224.0.0.0/3 -j DROP
    iptables -A OUTPUT -d 127.0.0.0/8 -o $interface -j DROP
    iptables -A OUTPUT -d 0.0.0.0/8 -j DROP
    iptables -A OUTPUT -d 100.64.0.0/10 -j DROP
    iptables -A OUTPUT -d 169.254.0.0/16 -j DROP
    iptables -A OUTPUT -d 192.0.0.0/24 -j DROP
    iptables -A OUTPUT -d 192.0.2.0/24 -j DROP
    iptables -A OUTPUT -d 198.18.0.0/15 -j DROP
    iptables -A OUTPUT -d 198.51.100.0/24 -j DROP
    iptables -A OUTPUT -d 203.0.113.0/24 -j DROP
    iptables -A OUTPUT -d 224.0.0.0/3 -j DROP
    iptables -A OUTPUT -s 127.0.0.0/8 -o $interface -j DROP
    iptables -A OUTPUT -s 0.0.0.0/8 -j DROP
    iptables -A OUTPUT -s 100.64.0.0/10 -j DROP
    iptables -A OUTPUT -s 169.254.0.0/16 -j DROP
    iptables -A OUTPUT -s 192.0.0.0/24 -j DROP
    iptables -A OUTPUT -s 192.0.2.0/24 -j DROP
    iptables -A OUTPUT -s 198.18.0.0/15 -j DROP
    iptables -A OUTPUT -s 198.51.100.0/24 -j DROP
    iptables -A OUTPUT -s 203.0.113.0/24 -j DROP
    iptables -A OUTPUT -s 224.0.0.0/3 -j DROP
    iptables -A INPUT -d 127.0.0.0/8 -i $interface -j DROP
    iptables -A INPUT -d 0.0.0.0/8 -j DROP
    iptables -A INPUT -d 100.64.0.0/10 -j DROP
    iptables -A INPUT -d 169.254.0.0/16 -j DROP
    iptables -A INPUT -d 192.0.0.0/24 -j DROP
    iptables -A INPUT -d 192.0.2.0/24 -j DROP
    iptables -A INPUT -d 198.18.0.0/15 -j DROP
    iptables -A INPUT -d 198.51.100.0/24 -j DROP
    iptables -A INPUT -d 203.0.113.0/24 -j DROP
    iptables -A INPUT -d 224.0.0.0/3 -j DROP
    iptables -A INPUT -i lo -j ACCEPT
    iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
    iptables -A INPUT -p tcp --match multiport --sports 1:1022 -m conntrack --ctstate ESTABLISHED -j ACCEPT
    iptables -A INPUT -p udp --match multiport --sports 1:1022 -m conntrack --ctstate ESTABLISHED -j ACCEPT
    iptables -A OUTPUT -p tcp --match multiport --dports 1:1022 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
    iptables -A OUTPUT -p udp --match multiport --dports 1:1022 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
    iptables -A OUTPUT -o lo -j ACCEPT
    iptables -P OUTPUT DROP
    iptables -A INPUT -p tcp --sport 80 -m conntrack --ctstate ESTABLISHED -j ACCEPT
    iptables -A INPUT -p tcp --sport 443 -m conntrack --ctstate ESTABLISHED -j ACCEPT
    iptables -A INPUT -p tcp --sport 53 -m conntrack --ctstate ESTABLISHED -j ACCEPT
    iptables -A INPUT -p udp --sport 53 -m conntrack --ctstate ESTABLISHED -j ACCEPT
    iptables -A OUTPUT -p tcp --dport 80 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
    iptables -A OUTPUT -p tcp --dport 443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
    iptables -A OUTPUT -p tcp --dport 53 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
    iptables -A OUTPUT -p udp --dport 53 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
    iptables -A OUTPUT -o lo -j ACCEPT
    iptables -P OUTPUT DROP
    mkdir /etc/iptables/
    touch /etc/iptables/rules.v4
    touch /etc/iptables/rules.v6
    iptables-save > /etc/iptables/rules.v4
    ip6tables-save > /etc/iptables/rules.v6

# sysctl
    sysctl -w net.ipv4.tcp_syncookies=1
    sysctl -w net.ipv4.ip_forward=0
    sysctl -w net.ipv4.conf.all.send_redirects=0
    sysctl -w net.ipv4.conf.default.send_redirects=0
    sysctl -w net.ipv4.conf.all.accept_redirects=0
    sysctl -w net.ipv4.conf.default.accept_redirects=0
    sysctl -w net.ipv4.conf.all.secure_redirects=0
    sysctl -w net.ipv4.conf.default.secure_redirects=0
    sysctl -w net.ipv4.ip_forward=0
    sysctl -w net.ipv4.conf.all.accept_source_route=0
    sysctl -w net.ipv4.conf.default.accept_source_route=
    sysctl -w net.ipv4.conf.all.accept_redirects=0
    sysctl -w net.ipv4.conf.default.accept_redirects=0
    sysctl -w net.ipv4.conf.all.secure_redirects=0
    sysctl -w net.ipv4.conf.default.secure_redirects=0
    sysctl -w net.ipv4.conf.all.log_martians=1
    sysctl -w net.ipv4.conf.default.log_martians=1
    sysctl -w net.ipv4.icmp_echo_ignore_broadcasts=1
    sysctl -w net.ipv4.icmp_ignore_bogus_error_responses=1
    sysctl -w net.ipv4.conf.all.rp_filter=1
    sysctl -w net.ipv4.conf.default.rp_filter=1
    sysctl -w net.ipv4.tcp_syncookies=1
    sysctl -w fs.suid_dumpable=0
    sysctl -w kernel.randomize_va_space=2
    sysctl -w net.ipv4.route.flush=1
    sysctl -p


# MySQL
echo -n "MySQL [Y/n] "
read option
if [[ $option =~ ^[Yy]$ ]]
then
  sudo apt-get -y install mysql-server  2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
  # Disable remote access
  sudo sed -i '/bind-address/ c\bind-address = 127.0.0.1' /etc/mysql/my.cnf
  sudo service mysql restart  2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
else
  sudo apt-get -y purge mysql*  2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
fi

# OpenSSH Server
echo -n "OpenSSH Server [Y/n] "
read option
if [[ $option =~ ^[Yy]$ ]]
then
  sudo apt-get -y install openssh-server  2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
  # Disable root login
  sudo sed -i '/^PermitRootLogin/ c\PermitRootLogin no' /etc/ssh/sshd_config
  sudo sed -i '/^Protocol/ c\Protocol 2' /etc/ssh/sshd_config
  sudo sed -i '/^IgnoreRhosts/ c\IgnoreRhosts yes' /etc/ssh/sshd_config
  sudo sed -i '/^HostbasedAuthentication/ c\HostbasedAuthentication no' /etc/ssh/sshd_config
  sudo sed -i '/^Banner/ c\Banner   /etc/issue' /etc/ssh/sshd_config
  sudo sed -i '/^PermitEmptyPasswords/ c\PermitEmptyPasswords no' /etc/ssh/sshd_config
  sudo sed -i '/^LogLevel/ c\LogLevel INFO' /etc/ssh/sshd_config
  sudo sed -i '/^UsePrivilegeSeparation/ c\UsePrivilegeSeparation yes' /etc/ssh/sshd_config
  sudo sed -i '/^StrictModes/ c\StrictModes yes' /etc/ssh/sshd_config
  sudo sed -i '/^VerifyReverseMapping/ c\VerifyReverseMapping yes' /etc/ssh/sshd_config
  sudo sed -i '/^AllowTcpForwarding/ c\AllowTcpForwarding no' /etc/ssh/sshd_config
  sudo sed -i '/^X11Forwarding/ c\X11Forwarding no' /etc/ssh/sshd_config
  sudo service ssh restart  2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
else
  sudo apt-get -y purge openssh-server*  2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
fi

# VSFTPD
echo -n "VSFTP [Y/n] "
read option
if [[ $option =~ ^[Yy]$ ]]
then
  sudo apt-get -y install vsftpd  2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
  # Disable anonymous uploads
  sudo sed -i '/^anon_upload_enable/ c\anon_upload_enable no' /etc/vsftpd.conf
  sudo sed -i '/^anonymous_enable/ c\anonymous_enable=NO' /etc/vsftpd.conf
  # FTP user directories use chroot
  sudo sed -i '/^chroot_local_user/ c\chroot_local_user=YES' /etc/vsftpd.conf
  sudo service vsftpd restart 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
else
  sudo apt-get -y purge vsftpd* 2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
fi

# VSFTPD
echo -n "Samba [Y/n] "
read option
if [[ $option =~ ^[Yy]$ ]]
then
  echo "You need to go secure samba somehow"
else
  sudo apt-get -y autoremove --purge samba*  2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
fi




echo -e "\nSetting apt sources list"
cat > /etc/apt/sources.list << EOF
###### Ubuntu Main Repos
deb http://archive.ubuntu.com/ubuntu/ xenial main restricted universe multiverse
deb-src http://archive.ubuntu.com/ubuntu/ xenial main restricted universe multiverse
###### Ubuntu Update Repos
deb http://archive.ubuntu.com/ubuntu/ xenial-security main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu/ xenial-updates main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu/ xenial-proposed main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu/ xenial-backports main restricted universe multiverse
deb-src http://archive.ubuntu.com/ubuntu/ xenial-security main restricted universe multiverse
deb-src http://archive.ubuntu.com/ubuntu/ xenial-updates main restricted universe multiverse
deb-src http://archive.ubuntu.com/ubuntu/ xenial-proposed main restricted universe multiverse
deb-src http://archive.ubuntu.com/ubuntu/ xenial-backports main restricted universe multiverse
###### Ubuntu Partner Repo
deb http://archive.canonical.com/ubuntu xenial partner
deb-src http://archive.canonical.com/ubuntu xenial partner
###### Ubuntu Extras Repo
deb http://extras.ubuntu.com/ubuntu xenial main
deb-src http://extras.ubuntu.com/ubuntu xenial main
EOF

#check for updates daily
echo -e "Enable check for updates daily"
cat > /etc/apt/apt.conf.d/10periodic << EOF
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "1";
APT::Periodic::Unattended-Upgrade "1";
EOF

# trees
    echo -e "\nTree of cron saved to cron.txt"
    apt-get -y install tree  2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
    tree /etc/cron.d > ~/Desktop/cron.txt
    tree /etc/cron.hourly >> ~/Desktop/cron.txt
    tree /etc/cron.daily >> ~/Desktop/cron.txt
    tree /etc/cron.weekly >> ~/Desktop/cron.txt
    tree /etc/cron.monthly >> ~/Desktop/cron.txt

# rc.local
    echo "exit 0" > /etc/rc.local

# lock out root user
    echo -e "\nLocking out root user, if it doesn't score points, unlock"
    sudo passwd -l root  2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput

# no guest account
    echo -e "\nDisabling guest account"
    echo "[SeatDefaults]" > /etc/lightdm/lightdm.conf
    echo "allow-guest=false" >> /etc/lightdm/lightdm.conf
    echo "greeter-hide-users = true" >> /etc/lightdm/lightdm.conf
    echo "greeter-show-manual-login = true" >> /etc/lightdm/lightdm.conf
    echo "autologin-user=none" >> /etc/lightdm/lightdm.conf

sleep 10
echo -e "\npreforming final apt upgrade\nPlease wait for this to finish"
xterm  -e 'apt-fast update'
xterm  -e 'apt-fast upgrade -y' &

cd ~
git clone https://github.com/CISOfy/lynis  2>> ~/Desktop/errorLog 1>> ~/Desktop/verboseOutput
cd lynis
sudo ./lynis audit system

#gui thingy
    sudo apt-get install zenity
    sudo chmod +x ~/Desktop/ubuntu-server-secure.sh
    sudo gksudo sh ~/Desktop/ubuntu-server-secure.sh
